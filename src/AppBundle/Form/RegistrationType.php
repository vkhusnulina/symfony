<?php

/**
 * Description of RegistrationFormType
 *
 * @author V.Khusnulina
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('first_name');
        $builder->add('last_name');
        $builder->add('company_name', null, array('mapped' => false));
    }

    public function getParent() {
        return 'fos_user_registration';
    }

    public function getName() {
        return 'company_registration';
    }

}
