<?php

namespace AppBundle\DBManager;

use AppBundle\DBManager\ConnectionString;

//TODO Delete this property
$cnn = False;

class DBManager {

    /**
     * Current DB Driver connector
     *
     * @var \AppBundle\DBManager\MysqliDriver
     */
    private $driver = null;
    private $_current_query = ''; // TODO For the moment unsed!

    const TENANT_DB = 'tenant_db';

    public function __construct($is_core_database = FALSE) {
        if ($is_core_database) {
            $this->startConnection(ConnectionString::getDefault());
        } else {
            $this->startConnection(ConnectionString::getTenant());
        }
    }

    public function __destruct() {
        $this->closeConnection();
    }

    public function startConnection($connection_str) {
        if ($this->driver == null) {
            $this->driver = new MysqliDriver($connection_str);
        }
    }

    public function closeConnection() {
        $this->driver->close();
    }

    public function insert($tableName, $data) {
        $response = [
            'result' => false,
            'message' => ''
        ];
        $insert_response = $this->driver->insert($tableName, $data);
        if ($insert_response['id'] == 0) {
            $response['message'] = $insert_response['error'];
        } else {
            $response['result'] = true;
            $response['message'] = $insert_response['id'];
        }
        return $response;
    }

    // TODO Rewrite - For the moment unsed!
    public function getLastQuery() {
        return $this->_current_query;
    }

    // TODO Add Exceptions control
    public function createNewSchema($new_database_name) {
        //Get New Database Location Data (From Core Database)
        $dbhost = ConnectionString::HOST;
        $dbuser = ConnectionString::USERNAME;
        $dbpass = ConnectionString::PASSWORD;
        $originalDB = self::TENANT_DB;

        $this->driver->selectDB($originalDB);
        $tablesList = $this->driver->query("SHOW TABLES");
        $this->driver->query("CREATE DATABASE `$new_database_name`");
        $this->driver->selectDB($new_database_name);
        foreach ($tablesList as $tbl) {
            $table = $tbl['Tables_in_' . $originalDB];
            $this->driver->query("CREATE TABLE $table LIKE " . $originalDB . "." . $table);
            $this->driver->query("INSERT INTO $table SELECT * FROM " . $originalDB . "." . $table);
        }
        return true;
    }

    // TODO Rewrite
    public function select($select = '*', $conditions = '') {
        global $cnn;
        $res = mysql_query($this->selectQuery($select, $conditions), $cnn);
        if ($res) {
            $r = [];
            // Keep semi-colon because it's just one line loop
            while ($r[] = mysql_fetch_array($res));
            array_pop($r);
            return $r;
        }
        return array();
    }

// TODO Rewrite
    private function selectQuery($select, $conditions) {
        $this->_current_query = 'SELECT ' . $select . ' FROM ' . $this->table . ' ' . $conditions . ';';
        return $this->_current_query;
    }

// TODO Rewrite
    private function startsWith($haystack, $needle) {
        return substr($haystack, 0, strlen($needle)) === $needle;
    }

// TODO Rewrite
    private function getVars() {
        $properties = get_object_vars($this);
        $prefix = $this->prefix;
        foreach ($properties as $key => $value) {
            if (!$this->startsWith($key, $prefix) || ($key == 'prefix') || ($value == NULL)) {
                unset($properties[$key]);
            }
        }
        return $properties;
    }

}
