<?php

namespace AppBundle\DBManager;

use Symfony\Component\HttpFoundation\Session\Session;

class ConnectionString {

    // TODO Get connection information from parameters
    const HOST = 'projectxeurope.clc3zhzm9zz1.eu-west-1.rds.amazonaws.com';
    const PORT = '3306';
    const USERNAME = 'masterroot';
    const PASSWORD = 'jkhF_34;';
    const DATABASE = 'projectx';

    public static function getDefault() {
        return [
            'host' => self::HOST /* . (self::PORT ? ':' . self::PORT : '') */,
            'username' => self::USERNAME,
            'password' => self::PASSWORD,
            'database' => self::DATABASE
        ];
    }

    public static function getTenant() {
        $ss = new Session();
        return [
            'host' => self::HOST, //$ss->get('host') /* . ($ss->get('port') ? ':' . $ss->get('port') : '') ,*/
            'username' => self::USERNAME, //$ss->get('username'),
            'password' => self::PASSWORD, //$ss->get('password'),
            'database' => $ss->get('schema')
        ];
    }

    public static function setHost($host) {
        $session = new Session();
        $session->set('host', $host);
    }

    public static function getHost() {
        $session = new Session();
        return $session->get('host');
    }

    public static function setPort($port) {
        $session = new Session();
        $session->set('port', $port);
    }

    public static function getPort() {
        $session = new Session();
        return $session->get('port');
    }

    public static function setUsername($user) {
        $session = new Session();
        $session->set('username', $user);
    }

    public static function getUsername() {
        $session = new Session();
        return $session->get('user');
    }

    public static function setPassword($pass) {
        $session = new Session();
        $session->set('password', $pass);
    }

    public static function getPassword() {
        $session = new Session();
        return $session->get('password');
    }

    public static function setDatabase($db) {
        $session = new Session();
        $session->set('database', $db);
    }

    public static function getDatabase() {
        $session = new Session();
        return $session->get('schema');
    }

}
