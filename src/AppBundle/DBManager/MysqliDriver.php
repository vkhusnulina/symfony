<?php

/**
 * Description of MysqliDriver
 *
 * @author V.Khusnulina
 */

namespace AppBundle\DBManager;

use AppBundle\SchemaStructures\Types;

class MysqliDriver {

    private $_conn;

    /**
     * Open new DB connection
     * @param array $connectionString   ['host' => string, 'username' => string, 'password' => string, 'database' => string]
     */
    public function __construct($connectionString) {
        $this->_conn = mysqli_init();
        if (!$this->_conn) {
            die('mysqli_init failed'); // TODO Exception manager
        }
        if (!$this->_conn->real_connect($connectionString['host'], $connectionString['username'], $connectionString['password'], $connectionString['database'])) {
            die('Connect Error (' . mysqli_connect_errno() . ') '
                    . mysqli_connect_error()); // TODO Exception manager
        }
        // To get connected host info: $this->_conn->host_info
    }

    /**
     * Close current opened connection
     */
    public function close() {
        $this->_conn->close();
        //echo "The connection is closed."; // TODO Delete this line
    }

    /**
     * Generate and execute INSERT statement
     * @param string $tableName
     * @param array $data   bound array ['columnName' => ['type_mysql', 'value']]
     * @return array    ['id' => int, 'error' => string]
     */
    public function insert($tableName, $data) {
        $response = [
            'id' => 0,
            'error' => ''
        ];
        $markers = $this->getMarkers(count($data));
        $columns = implode(',', array_keys($data));
        $query = "INSERT INTO $tableName ($columns) VALUES ($markers)";

        $stmt = $this->runPreparedQuery($query, $data);

        if ($stmt->errno > 0) {
            $response['error'] = $stmt->error . "\n";
        } else {
            $response['id'] = $stmt->insert_id;
        }

        $stmt->close();
        return $response;
    }

    // TODO Improve this method
    public function query($plainQuery) {
        $response = array();
        $result = $this->_conn->query($plainQuery);
        if ($result) {
            // Cycle through results
            if ($result !== true) {
                while ($row = $result->fetch_array()) {
                    $response[] = $row;
                }
                // Free result set
                $result->close();
            } else {
                $response = true;
            }
        } else {
            $response = false;
        }
        return $response;
    }

    public function selectDB($databaseName) {
        $this->_conn->select_db($databaseName);
    }

    // Get affected rows
    public function update($tableName, $data) {

    }

    /**
     * Run prepared query
     * @param string $query
     * @param array $params_r Array of ['type'=>string, 'value'=>string] (See "Type specification chars")
     * @link http://php.net/manual/en/mysqli-stmt.bind-param.php
     * @return \mysqli_stmt
     */
    private function runPreparedQuery($query, $params_r) {
        $stmt = $this->_conn->prepare($query);
        if ($stmt) {
            $this->bindParameters($stmt, $params_r);

            $stmt->execute();
        } else {
            var_dump($this->_conn->error_list);
            die();
        }
        return $stmt;
    }

    /**
     * Call bind_param for each array item
     * @param \mysqli_stmt $stmtObj
     * @param array $bind_params_r  Array of ['type'=>string, 'value'=>string] (See "Type specification chars") Types: s = string, i = integer, d = double, b = blob|unknown
     * @link http://php.net/manual/en/mysqli-stmt.bind-param.php
     */
    private function bindParameters(&$stmtObj, &$bind_params_r) {
        $types = '';
        array_walk($bind_params_r, function(&$item, $key) use (&$types) {
            $types.= Types::getParamType($item['type']);
            $item = $item['value'];
        });
        $referenced_params = $this->makeValuesReferenced($bind_params_r);
        call_user_func_array(array($stmtObj, "bind_param"), array_merge([$types], $referenced_params));
    }

    /**
     * Generate new array with referenced values
     * @param array $arr Array of column values by reference
     * @return array
     */
    private function makeValuesReferenced(&$arr) {
        $refs = array();
        foreach ($arr as $key => $value) {
            $refs[$key] = &$arr[$key];
        }
        return $refs;
    }

    /**
     * Generate string with markers separeted by comma
     * @param int $numberOfMarkers
     * @return string
     */
    private function getMarkers($numberOfMarkers) {
        return implode(',', array_fill(0, $numberOfMarkers, '?'));
    }

}
