<?php

/**
 * Description of DashboardController
 *
 * @author V.Khusnulina
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class DashboardController extends Controller {

    /**
     * @Route("/admin", name="dashboard")
     */
    public function indexAction() {
        return new RedirectResponse($this->generateUrl('invite_users'));
    }

    /**
     * @Route("/admin/invite_users", name="invite_users")
     */
    public function inviteUsersAction() {
        return $this->render('rapido/pages_blank_page.html.twig', array(
                    'base_dir' => realpath($this->container->getParameter('kernel.root_dir') . '/..'),
        ));
    }

}
