<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Models\Client;

class DefaultController extends Controller {

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction() {
        // replace this example code with whatever you need
        return $this->render('landing/main.html.twig', array(
                    'base_dir' => realpath($this->container->getParameter('kernel.root_dir') . '/..'),
        ));
    }

    /**
     * @Route("/test", name="test")
     */
    public function testAction() {
        echo "<pre>Test TableStructure + DBManager\n";

        echo "\nInserting into DB new Client\n";
        $client = new Client();
        $client->setName('MediaFem Group');
        $client->setCreator(12);
        $client->setTenantName();
        $result = $client->insert();
        echo "\nNew Client id: {$result['data']}\n";

        echo "</pre><pre>";
        echo "\nInserting into DB new Client, but with missing data...\n";
        $client2 = new Client();
        $client2->setName('Adtomatik');
        $client->setTenantName();
        $result2 = $client2->insert();
        if ($result2['valid']) {
            echo "\nNew Client id: {$result2['data']}\n";
        } else {
            echo "\nError: {$result2['message']} \n";
        }
        echo "\nTrying to set non-existent property...\n";
        $client->setSaraza('asd');

        echo "</pre>";
        die();

        // replace this example code with whatever you need
        return $this->render('landing/main.html.twig', array(
                    'base_dir' => realpath($this->container->getParameter('kernel.root_dir') . '/..'),
        ));
    }

    /**
     * @Route("/database", name="database")
     */
    public function databaseAction() {
        echo "<pre>Clone database\n";

        $dbManager = new \AppBundle\DBManager\DBManager(true);
        $dbManager->createNewSchema('prueba_' . date('U'));

        echo "</pre>";
        die();

        // replace this example code with whatever you need
        return $this->render('landing/main.html.twig', array(
                    'base_dir' => realpath($this->container->getParameter('kernel.root_dir') . '/..'),
        ));
    }

}
