<?php

namespace AppBundle\SchemaStructures;

/**
 * Description of Types
 *
 * @author V.Khusnulina
 */
class Types {

    const BIGINT = 'bigint';
    const BOOLEAN = 'boolean';
    const DATETIME = 'datetime';
    const DATE = 'date';
    const TIME = 'time';
    const TIMESTAMP = 'timestamp';
    const DECIMAL = 'decimal';
    const INTEGER = 'integer';
    const SMALLINT = 'smallint';
    const STRING = 'string';
    const TEXT = 'text';
    const BLOB = 'blob';
    const FLOAT = 'float';
    const ENUM = 'enum';

    /**
     * Mysqli bind types (See "Type specification chars")
     * @link http://php.net/manual/en/mysqli-stmt.bind-param.php
     * s = string, i = integer, d = double, b = blob|unknown
     */
    protected static $_paramTypeMap = array(
        self::BIGINT => 'i',
        self::INTEGER => 'i',
        self::SMALLINT => 'i',
        self::BOOLEAN => 'i',
        self::DECIMAL => 'd',
        self::FLOAT => 'd',
        self::STRING => 's',
        self::TEXT => 's',
        self::DATETIME => 's',
        self::DATE => 's',
        self::TIME => 's',
        self::TIMESTAMP => 's',
        self::ENUM => 's',
        self::BLOB => 'b',
    );

    public static function getParamType($type) {
        if (isset(self::$_paramTypeMap[$type])) {
            return self::$_paramTypeMap[$type];
        } else {
            return 'b'; //TODO Throw Exception
        }
    }

}
