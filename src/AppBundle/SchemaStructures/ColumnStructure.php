<?php

/**
 * Description of Columns
 *
 * @author V.Khusnulina
 */

namespace AppBundle\SchemaStructures;

class ColumnStructure {

    /**
     * All of the attributes set on the container.
     *
     * @var array
     */
    protected $attributes = [];

    /**
     * Create a new fluent container instance.
     *
     * @param  array|object    $attributes
     * @return void
     */
    public function __construct($attributes = []) {
        $this->setUpDefaultProperties();
        foreach ($attributes as $key => $value) {
            $this->attributes[$key] = $value;
        }
    }

    /**
     * Handle dynamic calls to the container to set attributes.
     *
     * @param  string  $method
     * @param  array   $parameters
     * @return $this
     */
    public function __call($method, $parameters) {
        $this->attributes[$method] = count($parameters) > 0 ? $parameters[0] : true;
        return $this;
    }

    /**
     * Dynamically retrieve the value of an attribute.
     * E.g. $column->property;
     *
     * @param  string  $key
     * @return mixed
     */
    public function __get($key) {
        return $this->get($key);
    }

    /**
     * Dynamically set the value of an attribute.
     * E.g. $column->property = true;
     *
     * @param  string  $key
     * @param  mixed   $value
     * @return void
     */
    public function __set($key, $value) {
        $this->attributes[$key] = $value;
    }

    /**
     * Get an attribute from the container.
     *
     * @param  string  $key
     * @return mixed
     */
    public function get($key) { // TODO Exception if attribute not exists
        if (array_key_exists($key, $this->attributes)) {
            return $this->attributes[$key];
        }
        return null;
    }

    /**
     * Set up default properties that all columns have to have
     */
    private function setUpDefaultProperties() {
        $this->attributes['value'] = null;
        $this->attributes['nullable'] = false;
        $this->attributes['readOnly'] = false;
    }

    public function toArray() {
        return $this->attributes;
    }

    /*     * **************************
     * ********* Validations
     */

    public function validate() {
        $response = [
            'valid' => true,
            'message' => ''
        ];
        if ($this->readOnly || ($this->nullable && is_null($this->value))) {
            //Skip validations if the attribute is ReadOnly
            //Or if the attr is Nullable and its current value is NULL
            return $response;
        }
        $response = $this->validateNullable();
        if ($response['valid']) {
            $response = $this->validateType();
        }
        return $response;
    }

    public static function validateAllColumns(array $columns) {
        $response = [
            'valid' => true,
            'errors' => array()
        ];

        foreach ($columns as $key => $column) {

            $validationResult = $column->validate();
            if (!$validationResult['valid']) {
                $response['valid'] = false;
                $response['errors'][$column->name] = $validationResult['message'];
            }
        }

        return $response;
    }

    protected function validateNullable() {
        return [
            'valid' => ((!$this->readOnly && !$this->nullable) && is_null($this->value)) ? false : true,
            'message' => "The property $this->name cannot be empty."
        ];
    }

    protected function validateLength() {
        return [
            'valid' => (isset($this->length) && (strlen($this->value) > $this->length)),
            'message' => "The maximum allowed length is " . (isset($this->length) ? isset($this->length) : 0) . "."
        ];
    }

    protected function validateInArray(array $allowed) {
        return [
            'valid' => in_array($this->value, $allowed),
            'message' => "This value could be " . implode(' or ', $allowed) . "."
        ];
    }

    protected function validateType() {
        $validationMethod = 'validate' . ucfirst($this->type) . 'Value';
        if (function_exists($validationMethod)) {
            return call_user_func_array($validationMethod);
        } else {
            return [
                'valid' => true,
                'message' => ''
            ];
        }
    }

    protected function validateStringValue() {
        if (!is_string($this->value)) {
            return [
                'valid' => false,
                'message' => 'The value must be a string.'
            ];
        }
        return $this->validateLength();
    }

    protected function validateEnumValue() {
        if (!is_string($this->value)) {
            return [
                'valid' => false,
                'message' => 'The value must be a string.'
            ];
        }
        return $this->validateInArray($this->allowed);
    }

    protected function validateBooleanValue() {
        if (!is_numeric($this->value)) {
            return [
                'valid' => false,
                'message' => 'The value must be a number.'
            ];
        }
        return $this->validateInArray([0, 1]);
    }

    protected function validateIntegerValue() {
        return [
            'valid' => is_numeric($this->value),
            'message' => 'The value must be a number.'
        ];
    }

    protected function validateBigintValue() {
        return [
            'valid' => is_numeric($this->value),
            'message' => 'The value must be a number.'
        ];
    }

}
