<?php

namespace AppBundle\SchemaStructures;

use AppBundle\SchemaStructures\ColumnStructure;
use AppBundle\SchemaStructures\Types;
use AppBundle\DBManager\DBManager;

abstract class TableStructure {

    /**
     * Database Manager
     *
     * @var DBManager
     */
    private $dbManager = null;

    /**
     * All columns releated to current table structure
     *
     * @var array [initially array of ColumnStructure]
     */
    public $columns = [];

    /**
     * Array of column names which ones have to be parsed as date
     *
     * @var array
     */
    protected $dateColumns = [];

    /**
     * Primary Key column name
     *
     * @var string
     */
    protected $primaryKey = null;

    /**
     * Table name used into current database
     *
     * @var string
     */
    protected $tableName = 'NOT_INITIALIZED';

    /**
     * Add deleted boolean column
     *
     * @var boolean
     */
    protected $softDelete = true;

    /**
     * Add created and updated timestamp columns
     *
     * @var boolean
     */
    protected $timestamps = true;

    /**
     * Create a new table structure manager instance.
     *
     * @param  boolean    $is_core_database false means to use tenant database, else use core database
     * @return void
     */
    function __construct($is_core_database = FALSE) {
        $this->dbManager = new DBManager($is_core_database);

        $this->_primaryKey();

        if ($this->softDelete) {
            $this->softDelete();
        }
        if ($this->timestamps) {
            $this->timestamps();
        }

        $this->setUpColumns();
    }

    /**
     * Permitted just for implicit getters and setters<br />
     * E.g. getCompleteName() will get complete_name value | setCompleteName($value) will set $value into complete_name
     * @param string $methodName
     * @param array $args
     * @return $methodName($args)
     */
    public function __call($methodName, $args) {
        $matches = array();
        if (preg_match('~^(set|get)([A-Z])(.*)$~', $methodName, $matches)) {
            $property = self::camelcase2underscore(strtolower($matches[2]) . $matches[3]);
            //TODO check if column exist
            if (!isset($this->columns[$property])) {
                // TODO manage exceptions
                var_dump('Property ' . $property . ' not exists');
                exit();
            }
            switch ($matches[1]) {
                case 'set':
                    $this->checkArguments($args, 1, 1, $methodName);
                    return $this->set($property, $args[0]);
                case 'get':
                    $this->checkArguments($args, 0, 0, $methodName);
                    return $this->get($property);
                default:
                    // TODO manage exceptions
                    var_dump('Method ' . $methodName . ' not exists');
                    exit();
            }
        }
    }

    /**
     * Retrieve column value
     * @param string $property  column name
     * @return mixed
     */
    protected function get($property) {
        return $this->columns[$property]->value;
    }

    /**
     * Set column value
     * @param string $property  column name
     * @param mixed $value
     * @return $value
     */
    protected function set($property, $value) {
        $this->columns[$property]->value = $value;
        return $this->columns[$property]->value;
    }

    /**
     * Filling $columns array with AppBundle\SchemaStructures\ColumnStructure items
     */
    abstract protected function setUpColumns();

    /**
     * Fetch key-value with columnName-value<br />
     * WARNING! Link all existent columns with its explicit values
     * @param array $values associative array
     * @return self
     */
    public function fill($values) {
        foreach ($values as $name => $value) {
            if (isset($this->columns[$name])/* && !$this->columns[$name]->readOnly */) {
                $this->columns[$name]->value = $value;
            }
        }
        return $this;
    }

    // TODO - Ask if there is another best way to do that
    public function toArray($excludeReadOnly = false, $excludeEmptyNullable = false) {
        $values = array();
        foreach ($this->columns as $name => $column) {
            if (!($excludeReadOnly && $column->readOnly) && !($excludeEmptyNullable && ($column->nullable && $column->value == null))) {
                $values[$name] = ['type' => $column->type, 'value' => $column->value];
            }
        }
        return $values;
    }

    // ------ INSERT ------------
    protected function preInsert() {
        return ['valid' => true, 'message' => "", 'data' => null];
    }

    protected function postInsert() {
        return ['valid' => true, 'message' => "", 'data' => null];
    }

    public function insert() {
        $response = [
            'valid' => false,
            'message' => "",
            'data' => null,
            'preResult' => null,
            'postResult' => null,
        ];

        $response['preResult'] = $this->preInsert();
        if ($response['preResult']['valid'] == false) {
            $response['message'] = "PreResultFailed";
            return $response;
        }
        $validation = ColumnStructure::validateAllColumns($this->columns);
        if ($validation['valid'] == false) {
            $response['valid'] = false;
            $response['message'] = implode("<br />", $validation['errors']); //TODO Save errors in array and manage them as are
            return $response;
        }
        $queryResult = $this->dbManager->insert($this->tableName, $this->toArray(true, true));
        $response['valid'] = $queryResult['result'];
        $response['message'] = $queryResult['message'];

        if ($response['valid']) {
            $response['data'] = $queryResult['message'];
            $response['postResult'] = $this->postInsert();
        }

        return $response;
    }

    // ------- SELECT ----------
    // TODO Rewrite
    public function getAll() {
        return $this->dbManager->select();
    }

    // ------- SHCEMA BUILD -----------
    // The reference code taken from: https://github.com/illuminate/database/blob/master/Schema/Blueprint.php

    /**
     * Create a new primary key big integer (8-byte) column for the table.
     *
     * @param  string  $column
     * @return AppBundle\SchemaStructures\ColumnStructure
     */
    public function _primaryKey($column = 'id') {
        unset($this->columns['id']);
        $this->primaryKey = $column;
        return $this->_bigInteger($column, true);
    }

    /**
     * Add created and updated timestamps columns.
     *
     * @return void
     */
    public function timestamps() {
        $this->_timestamp('created')->nullable()->readOnly();
        $this->_timestamp('updated')->nullable()->readOnly();
    }

    /**
     * Add deleted boolean column.
     *
     * @return void
     */
    public function softDelete() {
        $this->_boolean('deleted')->nullable();
    }

    /**
     * Create a new tiny integer (1-byte) column for the table.
     *
     * @param  string  $column
     * @param  bool  $autoIncrement
     * @return AppBundle\SchemaStructures\ColumnStructure
     */
    protected function _tinyInteger($column, $autoIncrement = false) {
        return $this->addColumn(Types::SMALLINT, $column, ($autoIncrement ? ['readOnly' => true, 'nullable' => true] : []));
    }

    /**
     * Create a new integer (4-byte) column for the table.
     *
     * @param  string  $column
     * @param  bool  $autoIncrement
     * @return AppBundle\SchemaStructures\ColumnStructure
     */
    protected function _integer($column, $autoIncrement = false) {
        return $this->addColumn(Types::INTEGER, $column, ($autoIncrement ? [ 'readOnly' => true, 'nullable' => true] : []));
    }

    /**
     * Create a new big integer (8-byte) column for the table.
     *
     * @param  string  $column
     * @param  bool  $autoIncrement
     * @return AppBundle\SchemaStructures\ColumnStructure
     */
    protected function _bigInteger($column, $autoIncrement = false) {
        return $this->addColumn(Types::BIGINT, $column, ($autoIncrement ? [ 'readOnly' => true, 'nullable' => true] : []));
    }

    /**
     * Create a new string column for the table.
     *
     * @param  string  $column
     * @param  int  $length
     * @return AppBundle\SchemaStructures\ColumnStructure
     */
    protected function _string($column, $length = 255) {
        return $this->addColumn(Types::STRING, $column, compact('length'));
    }

    /**
     * Create a new date column for the table.
     *
     * @param  string  $column
     * @return AppBundle\SchemaStructures\ColumnStructure
     */
    protected function _date($column) {
        $this->dateColumns[] = $column;
        return $this->addColumn(Types::DATE, $column);
    }

    /**
     * Create a new date-time column for the table.
     *
     * @param  string  $column
     * @return AppBundle\SchemaStructures\ColumnStructure
     */
    protected function _dateTime($column) {
        $this->dateColumns[] = $column;
        return $this->addColumn(Types::DATETIME, $column);
    }

    /**
     * Create a new timestamp column for the table.
     *
     * @param  string  $column
     * @return AppBundle\SchemaStructures\ColumnStructure
     */
    protected function _timestamp($column) {
        $this->dateColumns[] = $column;
        return $this->addColumn(Types::TIMESTAMP, $column);
    }

    /**
     * Create a new boolean column for the table.
     *
     * @param  string  $column
     * @return AppBundle\SchemaStructures\ColumnStructure
     */
    protected function _boolean($column) {
        return $this->addColumn(Types::BOOLEAN, $column);
    }

    /**
     * Create a new enum column for the table.
     *
     * @param  string  $column
     * @param  array   $allowed
     * @return AppBundle\SchemaStructures\ColumnStructure
     */
    protected function _enum($column, array $allowed) {
        return $this->addColumn(Types::ENUM, $column, compact('allowed'));
    }

    /**
     * Add a new column to the tableStructure.
     *
     * @param  string  $type
     * @param  string  $name
     * @param  array   $parameters
     * @return AppBundle\SchemaStructures\ColumnStructure
     */
    private function addColumn($type, $name, array $parameters = []) {
        $attributes = array_merge(compact('type', 'name'), $parameters);
        $this->columns[$name] = $column = new ColumnStructure($attributes);
        return $column;
    }

    /**
     * Check if the argument is mandatory or forbidden and throw exception if necessary
     * @param array $args
     * @param int $min
     * @param int $max
     * @param string $methodName
     */
    private function checkArguments(array $args, $min, $max, $methodName) {
        $argc = count($args);
        if ($argc < $min || $argc > $max) {
            // TODO manage exceptions
            echo 'Method ' . $methodName . ' needs minimaly ' . $min . ' and maximaly ' . $max . ' arguments. ' . $argc . ' arguments given.';
        }
    }

    private static function camelcase2underscore($name) {
        return strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $name));
    }

}
