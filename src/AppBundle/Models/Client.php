<?php

/**
 * Description of Company
 *
 * @author V.Khusnulina
 */

namespace AppBundle\Models;

use AppBundle\SchemaStructures\TableStructure;

class Client extends TableStructure {

    protected $tableName = 'Clients';

    public function __construct() {
        parent::__construct(true);
    }

    protected function setUpColumns() {
        $this->_dateTime('expire_date')->nullable();
        $this->_string('name', 50);
        $this->_string('slug', 50)->nullable();
        $this->_string('tenant_name', 50);
        $this->_bigInteger('creator');
    }

    public function setName($value) {
        $this->set('slug', $this->generateSlug($value));
        $this->set('name', $value);
        return $this->get('name');
    }

    public function setTenantName($value = null) {
        if (!$value) {
            $value = 'tenant_' . date('U');
        }
        $this->set('tenant_name', $value);
        return $this->get('tenant_name');
    }

    private function generateSlug($name) {
        $name = str_replace(" ", "_", $name);
        $name = preg_replace("/[^A-Za-z0-9_]/", "", $name);
        return strtolower($name);
    }

}
