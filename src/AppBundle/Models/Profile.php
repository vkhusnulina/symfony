<?php

namespace AppBundle\Models;

use AppBundle\SchemaStructures\TableStructure;

/**
 * Description of Profile
 *
 * @author V.Khusnulina
 */
class Profile extends TableStructure {

    protected $tableName = 'Profiles';

    protected function setUpColumns() {
        $this->_string('name', 50);
        $this->_bigInteger('creator');
    }

}
