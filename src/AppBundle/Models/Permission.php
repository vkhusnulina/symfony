<?php

namespace AppBundle\Models;

use AppBundle\SchemaStructures\TableStructure;

/**
 * Description of Permission
 *
 * @author V.Khusnulina
 */
class Permission extends TableStructure {

    protected $tableName = 'Permissions';

    public function __construct() {
        parent::__construct(true);
    }

    protected function setUpColumns() {
        $this->_bigInteger('feature_id');
        $this->_string('name', 50);
        $this->_string('description')->nullable();
    }

}
