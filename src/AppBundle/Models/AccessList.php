<?php

/**
 * Description of AccessList
 *
 * @author V.Khusnulina
 */

namespace AppBundle\Models;

use AppBundle\SchemaStructures\TableStructure;

class AccessList extends TableStructure {

    protected $tableName = 'AccessList';

    public function __construct() {
        parent::__construct(true);
    }

    protected function setUpColumns() {
        $this->_bigInteger('user_id');
        $this->_bigInteger('client_id');
        $this->_bigInteger('role_id');
        $this->_bigInteger('creator');
    }

}
