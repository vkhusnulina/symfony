<?php

namespace AppBundle\Models;

use AppBundle\SchemaStructures\TableStructure;

/**
 * Description of Role
 *
 * @author V.Khusnulina
 */
class Role extends TableStructure {

    protected $tableName = 'Roles';

    public function __construct() {
        parent::__construct(true);
    }

    protected function setUpColumns() {
        $this->_string('name', 50);
    }

}
