<?php

namespace AppBundle\Models;

use AppBundle\SchemaStructures\TableStructure;

/**
 * Description of FeatureAccess
 *
 * @author V.Khusnulina
 */
class FeatureAccess extends TableStructure {

    protected $tableName = 'FeaturesAccess';

    public function __construct() {
        parent::__construct(true);
    }

    protected function setUpColumns() {
        $this->_bigInteger('client_id');
        $this->_bigInteger('feature_id');
        $this->_dateTime('expire_date')->nullable();
        $this->_bigInteger('creator');
    }

}
