<?php

namespace AppBundle\Models;

use AppBundle\SchemaStructures\TableStructure;

/**
 * Description of Group
 *
 * @author V.Khusnulina
 */
class Group extends TableStructure {

    protected $tableName = 'Groups';

    protected function setUpColumns() {
        $this->_string('name', 50);
        $this->_string('description')->nullable();
        $this->_boolean('is_enabled')->nullable();
        $this->_bigInteger('creator');
    }

}
