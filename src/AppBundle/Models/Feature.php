<?php

namespace AppBundle\Models;

use AppBundle\SchemaStructures\TableStructure;

/**
 * Description of Feature
 *
 * @author V.Khusnulina
 */
class Feature extends TableStructure {

    protected $tableName = 'Features';

    public function __construct() {
        parent::__construct(true);
    }

    protected function setUpColumns() {
        $this->_string('name', 50);
        $this->_string('description')->nullable();
        $this->_boolean('is_premium')->nullable();
        $this->_boolean('is_enabled')->nullable();
    }

}
