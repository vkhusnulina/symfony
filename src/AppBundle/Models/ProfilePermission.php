<?php

namespace AppBundle\Models;

use AppBundle\SchemaStructures\TableStructure;

/**
 * Description of ProfilePermission
 *
 * @author V.Khusnulina
 */
class ProfilePermission extends TableStructure {

    protected $tableName = 'ProfilePermission';

    protected function setUpColumns() {
        $this->_bigInteger('group_id');
        $this->_bigInteger('profile_id');
        $this->_bigInteger('creator');
    }

}
