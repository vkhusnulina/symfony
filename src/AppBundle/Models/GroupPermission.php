<?php

namespace AppBundle\Models;

use AppBundle\SchemaStructures\TableStructure;

/**
 * Description of GroupPermission
 *
 * @author V.Khusnulina
 */
class GroupPermission extends TableStructure {

    protected $tableName = 'GroupPermission';

    protected function setUpColumns() {
        $this->_bigInteger('group_id');
        $this->_bigInteger('permission_id');
        $this->_bigInteger('creator');
    }

}
