<?php

namespace AppBundle;

use Symfony\Bundle\TwigBundle\TwigEngine;
use Mandrill as MandrillApi;

/**
 * Description of MailManager
 *
 * @author V.Khusnulina
 */
class MailManager {

    private $templator;
    private $api_key;
    private $sender_email;
    private $sender_name;

    public function __construct(TwigEngine $templating, $mandrill_key, $sender_email, $sender_name) {
        $this->templator = $templating;
        $this->api_key = $mandrill_key;
        $this->sender_email = $sender_email;
        $this->sender_name = $sender_name;
    }

    private function renderTemplate($template_name, $arguments = null) {
        return $this->templator->render('mail/' . $template_name . '.html.twig', $arguments);
    }

    public function setSender($name, $email) {
        $this->sender_name = $name;
        $this->sender_email = $email;
    }

    public function render() {
        return $this->sender_email . ' ' . $this->sender_name;
    }

    public function sendEmail($recipient_name, $recipient_email, $subject, $email_content) {
        try {
            $mandrillSender = new MandrillApi($this->api_key);
            $message = array(
                'html' => $email_content,
                'subject' => $subject,
                'from_email' => $this->sender_email,
                'from_name' => $this->sender_name,
                'to' => array(
                    array(
                        'email' => $recipient_email,
                        'name' => $recipient_name,
                        'type' => 'to'
                    )
                ),
                'headers' => array('Reply-To' => 'noreply@gmail.com'),
                'auto_text' => true
            );
            $async = true;
            $ip_pool = 'Main Pool';
            $result = $mandrillSender->messages->send($message, $async, $ip_pool);
            print_r($result); //Delete this line
        } catch (Mandrill_Error $e) {
            // Mandrill errors are thrown as exceptions
            echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
            // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
            throw $e;
        }
    }

    /*     * ******* Send Specific Emails ************ */

    public function sendEmailVerification($send_to_email, $send_to_name, $confirmationUrl) {
        $email_content = $this->renderTemplate('auth/verify_email', [
            'company' => $this->sender_name,
            'username' => $send_to_name,
            'confirmationUrl' => $confirmationUrl
        ]);
        $this->sendEmail($send_to_name, $send_to_email, 'Welcome to ' . $this->sender_name, $email_content);
    }

    public function sendPasswordRecover($send_to_email, $send_to_name, $confirmationUrl) {
        $email_content = $this->renderTemplate('auth/reset_password', [
            'company' => $this->sender_name,
            'username' => $send_to_name,
            'confirmationUrl' => $confirmationUrl
        ]);
        $this->sendEmail($send_to_name, $send_to_email, 'Reset Password ' . $this->sender_name, $email_content);
    }

}
