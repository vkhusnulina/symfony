<?php

/**
 * Description of ResettingController
 *
 * @author V.Khusnulina
 */

namespace ProcessfullyAuth\UserBundle\Controller;

use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Controller\ResettingController as BaseController;

class ResettingController extends BaseController {

    protected function getRedirectionUrl(UserInterface $user) {
        return $this->container->get('router')->generate('dashboard');
    }

}
