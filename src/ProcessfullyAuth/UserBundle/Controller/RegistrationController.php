<?php

/**
 * Description of RegistrationController
 *
 * @author V.Khusnulina
 */

namespace ProcessfullyAuth\UserBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\RegistrationController as BaseController;
use \AppBundle\DBManager\DBManager;
use AppBundle\Models\Client;
use AppBundle\Models\AccessList;

class RegistrationController extends BaseController {

    public function registerAction() {
        $form = $this->container->get('fos_user.registration.form');
        $formHandler = $this->container->get('fos_user.registration.form.handler');
        $confirmationEnabled = $this->container->getParameter('fos_user.registration.confirmation.enabled');

        $process = $formHandler->process($confirmationEnabled);
        if ($process) {

            $user = $form->getData();

            //Persists client data
            $client = new Client();
            $client->setName($form->get('company_name')->getData());
            $client->setCreator($user->getId());
            $client->setTenantName();
            $insertedClient = $client->insert();
            //Persists access_list data
            if ($insertedClient['valid']) {
                $dbManager = new DBManager(true);
                $dbManager->createNewSchema($client->getTenantName());
                $accessList = new AccessList();
                $accessList->fill([
                    'user_id' => $user->getId(),
                    'client_id' => $insertedClient['data'],
                    'role_id' => 1,
                    'creator' => $user->getId()
                ])->insert();
            } else {
                // TODO Manage Exception
                echo "\nError: {$insertedClient['message']} \n";
            }

            $authUser = false;
            if ($confirmationEnabled) {
                $this->container->get('session')->set('fos_user_send_confirmation_email/email', $user->getEmail());

                $route = 'fos_user_registration_check_email';
            } else {
                $authUser = true;
                $route = 'fos_user_registration_confirmed';
            }

            $this->setFlash('fos_user_success', 'registration.flash.user_created');
            $url = $this->container->get('router')->generate($route);
            $response = new RedirectResponse($url);

            if ($authUser) {
                $this->authenticateUser($user, $response);
            }

            return $response;
        }

        return $this->container->get('templating')->renderResponse('FOSUserBundle:Registration:register.html.' . $this->getEngine(), array(
                    'form' => $form->createView(),
        ));
    }

}
