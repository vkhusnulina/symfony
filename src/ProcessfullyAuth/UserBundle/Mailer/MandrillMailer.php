<?php

/**
 * Description of MandrillMailer
 *
 * @author V.Khusnulina
 */

namespace ProcessfullyAuth\UserBundle\Mailer;

use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Routing\RouterInterface;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Mailer\MailerInterface;

class MandrillMailer implements MailerInterface {

    /**
     * @var AppBundle/MailManager
     */
    protected $mailTransport;
    protected $router;
    protected $templating;
    protected $parameters;

    public function __construct($mailTransport, RouterInterface $router, EngineInterface $templating, array $parameters) {
        $this->mailTransport = $mailTransport;
        $this->router = $router;
        $this->templating = $templating;
        $this->parameters = $parameters;
    }

    public function sendConfirmationEmailMessage(UserInterface $user) {
        $url = $this->router->generate('fos_user_registration_confirm', array('token' => $user->getConfirmationToken()), true);
        $this->mailTransport->sendEmailVerification($user->getEmail(), $user->getName(), $url);
    }

    public function sendResettingEmailMessage(UserInterface $user) {
        $url = $this->router->generate('fos_user_resetting_reset', array('token' => $user->getConfirmationToken()), true);
        $this->mailTransport->sendPasswordRecover($user->getEmail(), $user->getName(), $url);
    }

}
