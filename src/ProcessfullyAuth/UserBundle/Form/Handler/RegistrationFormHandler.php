<?php

/**
 * Description of RegistrationFormHandler
 *
 * @author V.Khusnulina
 */

namespace ProcessfullyAuth\UserBundle\Form\Handler;

use FOS\UserBundle\Form\Handler\RegistrationFormHandler as BaseHandler;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Mailer\MailerInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Models\Client;

class RegistrationFormHandler extends BaseHandler {

    public function __construct(FormInterface $form, Request $request, UserManagerInterface $userManager, MailerInterface $mailer, TokenGeneratorInterface $tokenGenerator) {
        parent::__construct($form, $request, $userManager, $mailer, $tokenGenerator);
    }

    /**
     * @param boolean $confirmation
     */
    public function process($confirmation = false) {
        $user = $this->createUser();
        $this->form->setData($user);

        if ('POST' === $this->request->getMethod()) {
            $this->form->bind($this->request);
            if ($this->form->isValid()) {
                $this->onSuccess($user, $confirmation);

                return true;
            }
        }

        return false;
    }

}
