<?php

namespace ProcessfullyAuth\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Description of AcmeUserBundle
 *
 * @author V.Khusnulina
 */
class AcmeUserBundle extends Bundle {

    public function getParent() {
        return 'FOSUserBundle';
    }

}
