<?php

/**
 * Description of UserRegistrationListener
 *
 * @author V.Khusnulina
 */

namespace ProcessfullyAuth\UserBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;

class UserRegistrationListener implements EventSubscriberInterface {

    public static function getSubscribedEvents() {
        return array(
            FOSUserEvents::REGISTRATION_INITIALIZE => 'onRegistrationInit',
        );
    }

    /**
     * take action when registration is initialized
     * set the username to a unique id
     * @param FormEvent $userevent
     */
    public function onRegistrationInit(UserEvent $userevent) {
        $user = $userevent->getUser();
        $user->setUsername(uniqid());
    }

}
